### 1. Introduction
This repository acts as driver repo and contains input params and Jenkinsfile for pipeline which runs the module repos for example https://github.com/stakater/terraform-module-efs and https://github.com/stakater/terraform-module-aurora-db

### 2. Context
This repo is part of a parent project to achieve the possibility of controlled deployments in different environments so, that we can rollback

### 3. Problem Statement 
We need a driver pipeline which pulls the infrastructure modules and execute them in one job, then push the states to output repos.

### 4. Solution 

The solution is a stages based Jenkinsfile and Jenkins parameterized pipeline.

#### Technology Stack

* Jenkins
* Groovy
* Bash
* git

##### Infrastructure

This project can be used by a Jenkins parameterized pipeline.

### 5. Usage 

#### Running

##### Quickstart [RDS]

0. Make sure your Jenkins Slave running the job has github ssh keys mounted at `/root/.ssh-git/ssh-key`, which is mounted from `jenkins-git-ssh` secret. You can use the following manifest to add it (encode the text into base64 first).
_Note: You have to set the username and email of the git user [here](https://github.com/stakater/additional-infra-input-repo/blob/master/Jenkinsfile#L63) and [here](https://github.com/stakater/additional-infra-input-repo/blob/master/Jenkinsfile#L96)_
```
   - apiVersion: v1
     kind: Secret
     metadata:
       labels:
         app: jenkins
         provider: fabric8
         version: 4.0.27
         group: io.fabric8.tenant.apps
       name: jenkins-git-ssh
     data:
       ssh-key: ""
       ssh-key.pub: ""
     type: fabric8.io/jenkins-git-ssh
```

1. Create a parameterized pipeline in Jenkins from this repo. Make all of the following parameters of type String except boolean for `DEPLOY` which identifies if the pipeline should do CD along with CI.
    * AWS_ACCESS_KEY_ID
    * AWS_SECRET_ACCESS_KEY
    * REGION
    * DEPLOY
    * ACTION
    
_for creation ACTION would be "configure.yaml create.yaml" and for deletion, "configure.yaml delete.yaml"_

2. Set `organization` variable to your github user/organization which contains your input output and module repo. 

```
String organization = "stakater"
```
If you are using module repos from a different github account, let's say stakater, pass different value to the line fetching modules at [L#72](https://github.com/stakater/additional-infra-input-repo/blob/master/Jenkinsfile#L72)

```
71            for (int i = 0; i < moduleRepos.length; i++) {
72                checkoutRepo("stakater", moduleRepos[i], moduleBranches[i])
73            }
```
3. Modify the `outputRepo` and `outputRepoBranch` variables to point to output repo that would contain the state of the deployments, you need to create the specified repo in your github user/organization account

```
    String outputRepo = "output-repo-ref"
    String outputRepoBranch = "master"
```
4. Modify the `moduleRepos` and `moduleBranches` variables like this.

```
    // WARNING! These two arrays must be sync up.
    // ie. first index for first repo and its branch and so on
    String[] moduleRepos = ["terraform-module-rds"] as String[]
    String[] moduleBranches = ["master"] as String[]
```
_For more modules to be created in one job like efs just add their repo names as well in these arrays with their appropriate branches on corresponding indexes._

5. Update `tfvars.sh` to reflect vpc and stack details along with initial credentials for aurora-db
_Note: S3 backend bucket needs to be created first._

6. Fire up the pipeline.

##### Deleting

Run the pipeline again with ACTION = "configure.yaml delete.yaml"

### 6. Help 

**Got a question?** 
File a GitHub [issue](https://github.com/stakater/terraform-module-aurora-db/issues), send us an [email](stakater@gmail.com).

### 7. Contributing 


#### Bug Reports & Feature Requests

Please use the [issue tracker](https://github.com/stakater/terraform-module-aurora-db/issues) to report any bugs or file feature requests.

#### Developing

PRs are welcome. In general, we follow the "fork-and-pull" Git workflow.

 1. **Fork** the repo on GitHub
 2. **Clone** the project to your own machine
 3. **Commit** changes to your own branch
 4. **Push** your work back up to your fork
 5. Submit a **Pull request** so that we can review your changes

NOTE: Be sure to merge the latest from "upstream" before making a pull request!

### 8. Changelog 

View our closed [Pull Requests](https://github.com/stakater/terraform-module-aurora-db/pulls?q=is%3Apr+is%3Aclosed).

### 9. License 

Apache2 © [Stakater](https://stakater.com)
