#!/usr/bin/groovy
@Library('github.com/stakater/fabric8-pipeline-library@master')

String aws_access_key_id = ""
try {
    aws_access_key_id = AWS_ACCESS_KEY_ID
} catch (Throwable ignored) {
    aws_access_key_id = ""
}

String aws_secret_access_key = ""
try {
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY
} catch (Throwable ignored) {
    aws_secret_access_key = ""
}

Boolean isCD = false
try {
    isCD = Boolean.valueOf(DEPLOY)
} catch (Throwable ignored) {
    isCD = false
}

String database_password = ""
try {
    database_password = DATABASE_PASSWORD
} catch (Throwable ignored) {
    database_password = ""
}

String outputRepo = "git@gitlab.com:asim42/output-additional-modules.git"
String outputRepoBranch = "master"
String outputDir = "output-additional-modules"

String outputGitUser = "Muhammad Asim Siddique"
String outputGitEmail = "asim@aurorasolutions.io"

String[] moduleRepos = [
        "git@github.com:stakater/terraform-module-aurora-db.git",
] as String[]

String[] moduleBranches = [
        "dev",
] as String[]

String[] moduleDirs = [
        "terraform-module-aurora-db",
] as String[]

String[] moduleGitUsers = [
        "stakater-user",
] as String[]

String[] moduleGitEmails = [
        "stakater@gmail.com",
] as String[]

String inputFilesName = "tfvars.sh"

// This would be a temporary file
String secretsFile = "secrets.sh"

// will be retrieved from input file
String region = ""

clientsK8sNode(clientsImage: 'stakater/pipeline-tools:dev') {

    container('clients') {

        stage('Checkout Code') {
            checkout scm
        }

        stage('Prepare') {

            // write secrets to file
            sh """
                touch secrets.sh
                echo "\nexport TF_VAR_database_password=\"${database_password}\"" >> ${secretsFile}
            """

            setGitUserInfo(outputGitUser, outputGitEmail)

            // retrieve region from input file
            region = sh(returnStdout: true, script: """
                source ${inputFilesName}
                echo TF_VAR_region
            """).trim()

            persistAwsKeys(aws_access_key_id, aws_secret_access_key, region)
            addHostsToKnownHosts()

            checkoutRepo(outputRepo, outputRepoBranch, outputDir)
        }

        stage('CI: Validate') {
            for (int i = 0; i < moduleDirs.length; i++) {

                setGitUserInfo(moduleGitUsers[i], moduleGitEmails[i])
                addHostsToKnownHosts()
                checkoutRepo(moduleRepos[i], moduleBranches[i], moduleDirs[i])

                String buildDir = moduleDirs[i] + "/build/"
                String actionDir = moduleDirs[i] + "/ansible/"

                String playbookAction = "ansible-playbook configure.yaml"
                build(buildDir, moduleDirs[i], inputFilesName, actionDir, playbookAction, secretsFile, outputDir)
            }
        }

        if (isCD) {

            stage('CD: Deploy') {

                println("Deploy has been set to true. Applying CD")

                // Delete modules dirs
                sh "rm -rf terraform-module*/"
                for (int i = 0; i < moduleDirs.length; i++) {

                    addHostsToKnownHosts()
                    checkoutRepo(moduleRepos[i], moduleBranches[i], moduleDirs[i])

                    String buildDir = moduleDirs[i] + "/build/"
                    String actionDir = moduleDirs[i] + "/ansible/"

                    // retrieve action from module input file
                    String action = sh(returnStdout: true, script: """
                        source input-${moduleDirs[i]}/${inputFilesName}
                        echo \$action
                    """).trim()

                    String playbookAction = "ansible-playbook " + action
                    build(buildDir, moduleDirs[i], inputFilesName, actionDir, playbookAction, secretsFile, outputDir)
                }
            }

            stage('Clean') {
                sh "rm ${secretsFile}"
            }

            stage('Commit') {

                setGitUserInfo(outputGitUser, outputGitEmail)
                addHostsToKnownHosts()
                commitChanges(outputDir, "update infra state")
            }
        } else {

            println("Deploy has been set to false. Skipping CD")
        }
    }
}

def setGitUserInfo(String gitUserName, String gitUserEmail) {
    sh """
        git config --global user.name "${gitUserName}"
        git config --global user.email "${gitUserEmail}"
    """
}

def build(String buildDir, String moduleDir, String inputFile, String actionDir, String action, String secretsFile, String outputDir) {
    sh """
        cd ${WORKSPACE}
        mkdir -p ${buildDir}
        # copy additional input file to build dir
        cp input-${moduleDir}/${inputFile} ${buildDir}
        # append an extra line into the additional input file in build dir
        echo "\n" >> ${buildDir}/${inputFile}
        # now append input file into the additional input file in build dir
        cat ${inputFile} >> ${buildDir}/${inputFile}
        # copy secrets file into the build dir
        cp ${secretsFile} ${buildDir}/
        # perform the action
        cd ${actionDir}
        ${action}
        cd ${WORKSPACE}
        # delete secrets file from the build dir
        rm -f ${buildDir}/${secretsFile}
        # now copy build dir contents into output repo to be commited
        mkdir -p ${outputDir}/${moduleDir}
        cp -r ${buildDir}/* ${outputDir}/${moduleDir}
    """
}

def addHostsToKnownHosts() {
    sh """
        mkdir -p /root/.ssh/
        echo -e "Host github.com\\n\\tStrictHostKeyChecking no\\n" > /root/.ssh/config
        echo -e "Host gitlab.com\\n\\tStrictHostKeyChecking no\\n" >> /root/.ssh/config
        ssh-keyscan github.com > /root/.ssh/known_hosts
        echo "\n" >> /root/.ssh/known_hosts
        ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
    """
}

def commitChanges(String repoDir, String commitMessage) {
    // TODO: Find a solution for eval `ssh-agent -s` runned everytime
    // https://aurorasolutions.atlassian.net/browse/STK-11
    String messageToCheck = "nothing to commit, working tree clean"
    sh """
        chmod 600 /root/.ssh-git/ssh-key
        eval `ssh-agent -s`
        ssh-add /root/.ssh-git/ssh-key
        cd ${repoDir}
        git add .
        if ! git status | grep '${messageToCheck}' ; then
            git commit -m "${commitMessage}"
            git push
        else
            echo \"nothing to do\"
        fi
    """
}
def persistAwsKeys(String aws_access_key_id, String aws_secret_access_key, String aws_region) {
    sh """
        cd \$HOME
        mkdir -p .aws/
        echo "[default]\naws_access_key_id = ${aws_access_key_id}\naws_secret_access_key = ${aws_secret_access_key}" > .aws/credentials
        echo "[default]\nregion = ${aws_region}" > .aws/config
    """
}

def checkoutRepo(String repo, String branch, String dir) {
    // TODO: Find a solution for eval `ssh-agent -s` runned everytime
    // https://aurorasolutions.atlassian.net/browse/STK-11
    sh """
        chmod 600 /root/.ssh-git/ssh-key
        eval `ssh-agent -s`
        ssh-add /root/.ssh-git/ssh-key
        
        rm -rf ${dir}
        git clone -b ${branch} ${repo} ${dir}
    """
}
